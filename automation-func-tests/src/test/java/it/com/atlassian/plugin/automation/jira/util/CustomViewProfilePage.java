package it.com.atlassian.plugin.automation.jira.util;

import com.atlassian.jira.pageobjects.pages.ViewProfilePage;
import org.openqa.selenium.By;

/**
 * @author: mkonecny
 */
public class CustomViewProfilePage extends ViewProfilePage
{
    public String getTimezone()
    {
        return elementFinder.find(By.id("up-p-timezone-label")).getText();
    }
}
