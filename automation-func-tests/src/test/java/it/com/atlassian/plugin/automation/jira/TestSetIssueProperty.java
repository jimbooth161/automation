package it.com.atlassian.plugin.automation.jira;

import com.atlassian.jira.tests.TestBase;
import com.atlassian.plugin.automation.page.ActionsForm;
import com.atlassian.plugin.automation.page.AdminPage;
import com.atlassian.plugin.automation.page.CleanupRule;
import com.atlassian.plugin.automation.page.TriggerPage;
import com.atlassian.plugin.automation.page.action.CommentIssueActionForm;
import com.atlassian.plugin.automation.page.action.SetIssuePropertyForm;
import com.atlassian.plugin.automation.page.trigger.IssueEventTriggerForm;
import com.atlassian.plugin.automation.page.trigger.IssuePropertySetEventTriggerForm;
import com.meterware.httpunit.GetMethodWebRequest;
import com.meterware.httpunit.HttpUnitOptions;
import com.meterware.httpunit.WebResponse;
import it.com.atlassian.plugin.automation.jira.conditions.IssueCommentedCondition;
import it.com.atlassian.plugin.automation.jira.util.IntegrationTestVersionUtil;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import java.io.IOException;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.junit.Assert.assertEquals;

public class TestSetIssueProperty extends TestBase
{
    @org.junit.Rule
    public CleanupRule cleanup = new CleanupRule(jira());

    @Before
    public void setup()
    {
        jira().backdoor().dataImport().restoreDataFromResource("jira-automation-data.zip");
        jira().quickLoginAsAdmin();
    }

    @Test
    public void testSetIssueProperty() throws Exception
    {
        final int buildNumber = IntegrationTestVersionUtil.getJiraBuildNumber(jira().environmentData().getBaseUrl().toString());

        if (buildNumber >= IntegrationTestVersionUtil.JIRA_62_BUILD_NUMBER)
        {
            AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);

            TriggerPage triggerPage = adminPage.addRuleForm().
                    ruleName("set property").
                    enabled(true).
                    next();

            triggerPage.selectTrigger(IssueEventTriggerForm.class).
                    setEvents("Issue Created");
            ActionsForm actionsForm = triggerPage.next();
            final String propertyKey = "foo.blah";
            final int propertyValue = 1;
            actionsForm.setInitialAction(SetIssuePropertyForm.class).propertyKey(propertyKey).propertyValue(String.valueOf(propertyValue));
            adminPage = actionsForm.next().save();

            triggerPage = adminPage.addRuleForm().
                    ruleName("issue event property").
                    enabled(true).
                    next();

            triggerPage.selectTrigger(IssuePropertySetEventTriggerForm.class).
                    setEventPropertyKeys(propertyKey);
            actionsForm = triggerPage.next();
            actionsForm.setInitialAction(CommentIssueActionForm.class).comment("Added comment");
            adminPage = actionsForm.next().save();

            assertEquals("No rule was added", 2, adminPage.getRulesCount());

            final String issueKey = jira().backdoor().issues().createIssue("TEST", "Hello").key();

            // comment should be added due to the publish event action
            waitUntilTrue("comment should be added", new IssueCommentedCondition(jira().backdoor().issues(), issueKey, 1));

            final JSONObject issueProperty = getIssueProperty(issueKey, propertyKey);
            assertEquals("property not set correctly", propertyValue, issueProperty.getInt("value"));
        }
    }

    public JSONObject getIssueProperty(String issueKey, String propertyKey) throws Exception
    {
        final String url = String.format("%s/rest/api/2/issue/%s/properties/%s", jira().environmentData().getBaseUrl(), issueKey, propertyKey);
        funcTestHelper.webTester.getDialog().getWebClient().setExceptionsThrownOnErrorStatus(false);
        HttpUnitOptions.setExceptionsThrownOnErrorStatus(false);

        try
        {
            final WebResponse response = funcTestHelper.webTester.getDialog().getWebClient().sendRequest(new GetMethodWebRequest(url));
            return new JSONObject(IOUtils.toString(response.getInputStream()));
        }
        catch (IOException e)
        {
            return null;
        }
        catch (SAXException e)
        {
            return null;
        }
    }
}
