package com.atlassian.plugin.automation.admin;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.LocaleResolver;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import static com.google.common.collect.Maps.newHashMap;

@Scanned
public class RulesServlet extends AbstractAdminServlet
{
    private final LocaleResolver localeResolver;

    @Inject
    public RulesServlet(
            @ComponentImport final WebSudoManager webSudoManager,
            @ComponentImport final SoyTemplateRenderer renderer,
            @ComponentImport final UserManager userManager,
            @ComponentImport final LoginUriProvider loginUriProvider,
            @ComponentImport final WebResourceManager webResourceManager,
            @ComponentImport final LocaleResolver localeResolver)
    {
        super(webSudoManager, renderer, userManager, loginUriProvider, webResourceManager);
        this.localeResolver = localeResolver;
    }

    @Override
    protected void requireResource(WebResourceManager webResourceManager)
    {
        webResourceManager.requireResource(COMPLETE_PLUGIN_KEY + ":rule-list-resources");
    }

    @Override
    protected void renderResponse(SoyTemplateRenderer renderer, HttpServletRequest request, HttpServletResponse response)
            throws IOException, SoyException
    {
        final Map<String, Object> context = newHashMap();
        final SimpleDateFormat format = new SimpleDateFormat("dd/MM h:mm a", localeResolver.getLocale());
        context.put("currentServerTime", format.format(new Date()));
        renderer.render(response.getWriter(), CONFIG_RESOURCE_KEY, "Atlassian.Templates.Automation.rulePage", context);
    }
}