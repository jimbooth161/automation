package com.atlassian.plugin.automation.config;

import com.atlassian.plugin.automation.core.AutomationConfiguration;
import com.atlassian.plugin.automation.core.action.ActionConfiguration;
import com.atlassian.plugin.automation.core.trigger.TriggerConfiguration;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;
import java.util.Map;

public class DefaultAutomationConfiguration implements AutomationConfiguration, ActionConfiguration, TriggerConfiguration
{
    @JsonProperty
    private Integer id;

    @JsonProperty
    private final String moduleKey;

    @JsonProperty
    private final Map<String, List<String>> params;

    public DefaultAutomationConfiguration(@JsonProperty("id") final int id,
                                          @JsonProperty("moduleKey") final String moduleKey,
                                          @JsonProperty("params") final Map<String, List<String>> params)
    {
        this.id = id;
        this.moduleKey = moduleKey;
        this.params = params;
    }

    @Override
    public Integer getId()
    {
        return id;
    }

    @Override
    public String getModuleKey()
    {
        return moduleKey;
    }

    @Override
    public Map<String, List<String>> getParameters()
    {
        return params;
    }

    public void unsetId()
    {
        id = null;
    }
}
