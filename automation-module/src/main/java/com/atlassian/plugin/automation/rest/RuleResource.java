package com.atlassian.plugin.automation.rest;

import com.atlassian.fugue.Either;
import com.atlassian.plugin.automation.config.DefaultRule;
import com.atlassian.plugin.automation.core.Action;
import com.atlassian.plugin.automation.core.Rule;
import com.atlassian.plugin.automation.core.Trigger;
import com.atlassian.plugin.automation.core.action.ActionConfiguration;
import com.atlassian.plugin.automation.core.trigger.TriggerConfiguration;
import com.atlassian.plugin.automation.module.AutomationModuleManager;
import com.atlassian.plugin.automation.service.RuleErrors;
import com.atlassian.plugin.automation.service.RuleService;
import com.atlassian.plugin.automation.status.RuleStatusService;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.log4j.Logger;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


/**
 * Provides REST resource for configuring the rules
 */
@Path ("rule")
@Produces ({ MediaType.APPLICATION_JSON })
@Consumes ({ MediaType.APPLICATION_JSON })
@WebSudoRequired
public class RuleResource
{
    private static final Logger log = Logger.getLogger(RuleResource.class);

    private final RuleService ruleService;
    private final UserManager userManager;
    private final RuleStatusService ruleStatusService;
    private final AutomationModuleManager automationModuleManager;

    @Inject
    public RuleResource(
            @ComponentImport final UserManager userManager,
            final RuleService ruleService,
            final RuleStatusService ruleStatusService,
            final AutomationModuleManager automationModuleManager)
    {
        this.ruleService = ruleService;
        this.userManager = userManager;
        this.ruleStatusService = ruleStatusService;
        this.automationModuleManager = automationModuleManager;
    }

    @POST
    public Response createRule(final DefaultRule rule)
    {
        final Either<RuleErrors, RuleService.RuleValidationResult> validationResult =
                ruleService.validateAddRule(userManager.getRemoteUsername(), rule);
        return validationResult.fold(new HandleRuleErrorFunction(), new Function<RuleService.RuleValidationResult, Response>()
        {
            @Override
            public Response apply(@Nullable final RuleService.RuleValidationResult input)
            {
                if (input != null)
                {
                    return Response.ok(convertRuleToView(ruleService.addRule(input))).build();
                }
                return Response.status(javax.ws.rs.core.Response.Status.NOT_FOUND).build();
            }
        });
    }

    @POST
    @Path ("validate/{step}")
    public Response validatePartialRule(final DefaultRule rule, @PathParam ("step") final int step)
    {
        final RuleErrors validationResult =
                ruleService.validatePartialRule(userManager.getRemoteUsername(), rule, step);
        if (validationResult.hasAnyErrors())
        {
            return new HandleRuleErrorFunction().apply(validationResult);
        }
        return Response.ok(convertRuleToView(rule)).build();
    }

    @PUT
    @Path ("{ruleId}")
    public Response updateRule(final DefaultRule rule)
    {
        final Either<RuleErrors, RuleService.RuleValidationResult> validationResult =
                ruleService.validateUpdateRule(userManager.getRemoteUsername(), rule);
        return validationResult.fold(new HandleRuleErrorFunction(), new Function<RuleService.RuleValidationResult, Response>()
        {
            @Override
            public Response apply(@Nullable final RuleService.RuleValidationResult input)
            {
                if (input != null)
                {
                    return Response.ok(convertRuleToView(ruleService.updateRule(input))).build();
                }
                return Response.status(javax.ws.rs.core.Response.Status.NOT_FOUND).build();
            }
        });
    }

    @GET
    @Path ("{ruleId}")
    public Response getRule(@PathParam ("ruleId") final int ruleId)
    {
        Either<ErrorCollection, Rule> result = ruleService.getRule(userManager.getRemoteUsername(), ruleId);
        return result.fold(new HandleErrorFunction(), new Function<Rule, Response>()
        {
            @Override
            public Response apply(@Nullable final Rule input)
            {
                if (input != null)
                {
                    return Response.ok(convertRuleToView(input)).build();
                }
                return Response.status(javax.ws.rs.core.Response.Status.NOT_FOUND).build();
            }
        });
    }

    @DELETE
    @Path ("{ruleId}")
    public Response deleteRule(@PathParam ("ruleId") final int ruleId)
    {
        Either<ErrorCollection, RuleService.DeleteRuleValidationResult> result = ruleService.validateDeleteRule(userManager.getRemoteUsername(), ruleId);

        return result.fold(new HandleErrorFunction(), new Function<RuleService.DeleteRuleValidationResult, Response>()
        {
            @Override
            public Response apply(@Nullable RuleService.DeleteRuleValidationResult input)
            {
                if (input != null)
                {
                    ruleService.deleteRule(input);
                    return Response.ok().build();
                }
                return Response.status(javax.ws.rs.core.Response.Status.NOT_FOUND).build();
            }
        }
        );
    }

    @PUT
    @Path ("{ruleId}/enable")
    public Response enableRule(@PathParam ("ruleId") final int ruleId)
    {
        Either<ErrorCollection, RuleService.UpdateRuleStatusValidationResult> result = ruleService.validateUpdateRuleStatus(userManager.getRemoteUsername(), ruleId);

        return result.fold(new HandleErrorFunction(), new Function<RuleService.UpdateRuleStatusValidationResult, Response>()
        {
            @Override
            public Response apply(@Nullable RuleService.UpdateRuleStatusValidationResult input)
            {
                if (input != null)
                {
                    final Rule rule = ruleService.updateRuleStatus(input, true);
                    return Response.ok(convertRuleToView(rule)).build();
                }
                return Response.status(javax.ws.rs.core.Response.Status.NOT_FOUND).build();
            }
        }
        );
    }

    @PUT
    @Path ("{ruleId}/disable")
    public Response disableRule(@PathParam ("ruleId") final int ruleId)
    {
        Either<ErrorCollection, RuleService.UpdateRuleStatusValidationResult> result = ruleService.validateUpdateRuleStatus(userManager.getRemoteUsername(), ruleId);

        return result.fold(new HandleErrorFunction(), new Function<RuleService.UpdateRuleStatusValidationResult, Response>()
        {
            @Override
            public Response apply(@Nullable RuleService.UpdateRuleStatusValidationResult input)
            {
                if (input != null)
                {
                    final Rule rule = ruleService.updateRuleStatus(input, false);
                    return Response.ok(convertRuleToView(rule)).build();
                }
                return Response.status(javax.ws.rs.core.Response.Status.NOT_FOUND).build();
            }
        }
        );
    }

    @GET
    public Response getAllRules()
    {
        Either<ErrorCollection, ? extends Iterable<Rule>> result = ruleService.getRules(userManager.getRemoteUsername());
        return result.fold(new HandleErrorFunction(), new Function<Iterable<Rule>, Response>()
        {
            @Override
            public Response apply(final Iterable<Rule> input)
            {
                final Iterable<RuleView> ruleViews = Iterables.transform(input, new Function<Rule, RuleView>()
                {
                    @Override
                    public RuleView apply(Rule input)
                    {
                        return convertRuleToView(input);
                    }
                });
                return Response.ok(Iterables.toArray(ruleViews, RuleView.class)).build();
            }
        });
    }

    @GET
    @Path("export/{ruleId}")
    public Response exportRule(@PathParam("ruleId") final int ruleId, @Context final HttpServletResponse response)
    {
        Either<ErrorCollection, Rule> result = ruleService.getRule(userManager.getRemoteUsername(), ruleId);
        return result.fold(new HandleErrorFunction(), new Function<Rule, Response>()
        {
            @Override
            public Response apply(@Nullable final Rule input)
            {
                if (input != null)
                {
                    DefaultRule rule = ((DefaultRule) input).unsetAllIds();
                    return Response.ok(rule).
                            header("Content-Description", "File Transfer").
                            header("Content-Disposition", "attachment; filename=" + rule.getName()).
                            build();
                }
                return Response.status(javax.ws.rs.core.Response.Status.NOT_FOUND).build();
            }
        });
    }

    @POST
    @Path("import")
    public Response importRule(final DefaultRule rule)
    {
        rule.unsetAllIds();
        final Either<RuleErrors, RuleService.RuleValidationResult> validationResult =
                ruleService.validateAddRule(userManager.getRemoteUsername(), rule);
        return validationResult.fold(new HandleRuleErrorFunction(), new Function<RuleService.RuleValidationResult, Response>()
        {
            @Override
            public Response apply(@Nullable final RuleService.RuleValidationResult input)
            {
                if (input != null)
                {
                    return Response.ok(convertRuleToView(ruleService.addRule(input))).build();
                }
                return Response.status(javax.ws.rs.core.Response.Status.NOT_FOUND).build();
            }
        });
    }

    private RuleView convertRuleToView(final Rule input)
    {
        final String triggerViewHtml = renderTriggerView(input.getTriggerConfiguration(), input.getActor());
        final ViewContainer triggerView = new ViewContainer(input.getTriggerConfiguration().getId(), input.getTriggerConfiguration().getModuleKey(), triggerViewHtml);
        final List<ViewContainer> actionViews = Lists.newArrayList();

        for (final ActionConfiguration actionConfiguration : input.getActionsConfiguration())
        {
            actionViews.add(new ViewContainer(actionConfiguration.getId(), actionConfiguration.getModuleKey(), renderActionView(actionConfiguration, input.getActor())));
        }

        final RuleWithStatusWrapper ruleWithStatus = new RuleWithStatusWrapper(input, ruleStatusService.getRuleStatus(input.getId()));
        return new RuleView(ruleWithStatus, triggerView, actionViews);
    }


    private String renderTriggerView(final TriggerConfiguration triggerConfiguration, final String actor)
    {
        try
        {
            final Trigger trigger = automationModuleManager.getTrigger(triggerConfiguration.getModuleKey());
            if (trigger == null)
            {
                return null;
            }
            // There be dragons - if NPE or anything happens in any plugin, we still want to be able to render something
            return trigger.getViewTemplate(triggerConfiguration, actor);
        }
        catch (Exception e)
        {
            final String message = "Rendering the view template threw an exception. Trigger: " + triggerConfiguration.getModuleKey();
            log.error(message, e);
            return message;
        }
    }

    private String renderActionView(final ActionConfiguration actionConfiguration, final String actor)
    {
        try
        {
            final Action action = automationModuleManager.getAction(actionConfiguration.getModuleKey());
            if (action == null)
            {
                return null;
            }
            // There be dragons - if NPE or anything happens in any plugin, we still want to be able to render something
            return action.getViewTemplate(actionConfiguration, actor);
        }
        catch (Exception e)
        {
            final String message = "Rendering the view template threw an exception. Action: " + actionConfiguration.getModuleKey();
            log.error(message, e);
            return message;
        }
    }
}
