package com.atlassian.plugin.automation.scheduler;

import com.atlassian.plugin.automation.auditlog.AdminAuditLogService;
import com.atlassian.plugin.automation.auditlog.AuditLogService;
import com.atlassian.plugin.automation.auditlog.EventAuditLogService;
import com.atlassian.plugin.automation.config.RuleConfigStore;
import com.atlassian.plugin.automation.core.Rule;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.plugin.event.events.PluginModuleDisabledEvent;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.scheduling.PluginJob;
import com.atlassian.sal.api.scheduling.PluginScheduler;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.google.common.collect.Maps;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Date;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import static java.lang.Thread.sleep;

/**
 * This class is responsible for initializing the scheduler and populating it with existing rules
 * <p/>
 * Be *VERY* careful when changing the lifecycle of this class and make sure you understand the Atlassian plugin
 * lifecycle: https://developer.atlassian.com/display/JIRADEV/JIRA+Plugin+Lifecycle
 */
@Named
public class SchedulerInitializer implements DisposableBean
{
    private static final Logger log = Logger.getLogger(SchedulerInitializer.class);

    private static final String PLUGIN_KEY = "com.atlassian.plugin.automation.automation-module";

    // This refers to key of "some" module in this plugin. We cannot intercept PluginDisabledEvent within the same plugin and we
    // need to do some teardown (see https://developer.atlassian.com/display/JIRADEV/JIRA+Plugin+Lifecycle#JIRAPluginLifecycle-PluginDisabledEventandPluginModuleDisabledEvent)
    // Because this class is in transformless plugin, there is no named module which will be disabled so we use another one which IS a named module
    private static final String THIS_PLUGINS_MODULE_KEY = PLUGIN_KEY + ":automation-rest";

    private static final long TRUNCATE_INTERVAL_MINUTES = 24 * 60;
    private static final String TRUNCATE_JOB_NAME = "AutomationAuditLogTruncateJob";
    private static final String TRUNCATE_JOB_SERVICES_KEY = "TRUNCATE_JOB_SERVICES_KEY";
    private static final String TRUNCATE_JOB_TRANSACTION_TEMPLATE = "TRUNCATE_JOB_TRANSACTION_TEMPLATE";
    private static final int STARTUP_DELAY = 20000;
    public static final int MAX_RETRY_COUNT = 5;

    private final AutomationScheduler automationScheduler;
    private final RuleConfigStore ruleStore;
    private final TransactionTemplate transactionTemplate;
    private final PluginEventManager pluginEventManager;
    private final PluginScheduler pluginScheduler;
    private final EventAuditLogService eventAuditLogService;
    private final AdminAuditLogService adminAuditLogService;
    private final Semaphore scheduled = new Semaphore(1, true);
    private final AtomicBoolean started = new AtomicBoolean(false);

    @Inject
    public SchedulerInitializer(
            @ComponentImport final PluginScheduler pluginScheduler,
            @ComponentImport final TransactionTemplate transactionTemplate,
            @ComponentImport final PluginEventManager pluginEventManager,
            final AutomationScheduler automationScheduler,
            final RuleConfigStore ruleStore,
            final EventAuditLogService eventAuditLogService,
            final AdminAuditLogService adminAuditLogService)
    {
        this.automationScheduler = automationScheduler;
        this.ruleStore = ruleStore;
        this.pluginScheduler = pluginScheduler;
        this.eventAuditLogService = eventAuditLogService;
        this.adminAuditLogService = adminAuditLogService;
        this.transactionTemplate = transactionTemplate;
        this.pluginEventManager = pluginEventManager;

        this.pluginEventManager.register(this);
    }

    @PluginEventListener
    public void start(final PluginEnabledEvent pluginEnabledEvent)
    {
        if (pluginEnabledEvent.getPlugin().getKey().equals(PLUGIN_KEY))
        {
            try
            {
                if (scheduled.tryAcquire(STARTUP_DELAY, TimeUnit.MILLISECONDS))
                {
                    //this is not great but due to the fact that plugin initialisation is kinda fucked in our products
                    //we have to wait a bit here until everything's started up before calling AO and scheduling jobs etc!
                    final Timer timer = new Timer();
                    timer.schedule(new TimerTask()
                    {
                        @Override
                        public void run()
                        {
                            try
                            {
                                start();
                            }
                            finally
                            {
                                scheduled.release();
                            }
                        }
                    }, STARTUP_DELAY);
                }
            }
            catch (InterruptedException e)
            {
                log.error("Unexpected error while starting automation scheduler.", e);
            }
        }
    }

    @PluginEventListener
    public void shutdown(final PluginModuleDisabledEvent moduleDisabledEvent)
    {
        if (moduleDisabledEvent.getModule().getCompleteKey().equals(THIS_PLUGINS_MODULE_KEY))
        {
            try
            {
                //wait for the start() timer to schedule its stuff first otherwise we could get into a situation
                //where it runs after we've tried to unscheduled things resulting in an orphaned plugin job
                //and automation rules.
                if (scheduled.tryAcquire(STARTUP_DELAY, TimeUnit.MILLISECONDS))
                {
                    shutdown();
                }
            }
            catch (InterruptedException e)
            {
                log.error("Unexpected error while stopping automation scheduler.", e);
            }
            finally
            {
                pluginEventManager.unregister(this);
                scheduled.release();
            }
        }
    }

    public void start()
    {
        if (started.compareAndSet(false, true))
        {
            try
            {
                final Map<String, Object> jobData = Maps.newHashMap();
                jobData.put(TRUNCATE_JOB_SERVICES_KEY, new AuditLogService[]{eventAuditLogService, adminAuditLogService});
                jobData.put(TRUNCATE_JOB_TRANSACTION_TEMPLATE, transactionTemplate);

                callWithRetry(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        pluginScheduler.scheduleJob(TRUNCATE_JOB_NAME, AutomationLogTruncateJob.class,
                                jobData,
                                new Date(),
                                TRUNCATE_INTERVAL_MINUTES * 60000L);
                    }
                }, MAX_RETRY_COUNT, "Unable to schedule the automation truncate log job");

                //this doesn't execute in the context of a web-transaction so need to use our own
                //transaction template here!
                transactionTemplate.execute(new TransactionCallback<Object>()
                {
                    @Override
                    public Object doInTransaction()
                    {
                        for (final Rule rule : ruleStore.getRules())
                        {
                            callWithRetry(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    automationScheduler.scheduleRule(rule);
                                }
                            }, MAX_RETRY_COUNT, "Unexpected error while scheduling rule: " + rule.getName());
                        }
                        return null;
                    }
                });
            }
            catch (Throwable t)
            {
                log.error("Unexpected error while starting automation scheduler.", t);
            }
        }
    }

    public void shutdown()
    {
        try
        {
            if (started.compareAndSet(true, false))
            {
                pluginScheduler.unscheduleJob(TRUNCATE_JOB_NAME);
                automationScheduler.unscheduleAllRules();
            }
        }
        catch (RuntimeException e)
        {
            log.error("Unexpected error while stopping automation scheduler.", e);
        }
    }

    @Override
    public void destroy() throws Exception
    {
        pluginEventManager.unregister(this);
    }

    /**
     * This is a poor attempt to overcome the initialization issues imposed by non-existent way of detecting if the application (JIRA, Confluence) is
     * running. We try to execute the Runnable and if it fails, we just wait and try it again.
     */
    private void callWithRetry(final Runnable r, final int maxRetryCount, final String logMessage)
    {
        int retryCount = 0;
        while (retryCount < maxRetryCount)
        {
            try
            {
                r.run();
                return;
            }
            catch (Throwable t)
            {
                // this is awkward - if it fails to execute, we will just give it another try
                log.info(String.format("%s. Retry %d of %d", logMessage, retryCount, maxRetryCount), t);
                try
                {
                    // sleep to give the retry more chance
                    sleep((retryCount + 1) * 1000);
                }
                catch (InterruptedException e)
                {
                    // swallow the exception
                }
            }
            finally
            {
                // increase the retry count in the finally so we eventually get out of the loop
                retryCount++;
            }
        }
        log.error(String.format("Unable to start: %s after %d retries", logMessage, maxRetryCount));
    }

    public static class AutomationLogTruncateJob implements PluginJob
    {
        @Override
        public void execute(Map<String, Object> jobDataMap)
        {
            final TransactionTemplate transactionTemplate = (TransactionTemplate) jobDataMap.get(TRUNCATE_JOB_TRANSACTION_TEMPLATE);
            final AuditLogService[] auditLogServices = (AuditLogService[]) jobDataMap.get(TRUNCATE_JOB_SERVICES_KEY);
            for (final AuditLogService auditLogService : auditLogServices)
            {
                transactionTemplate.execute(new TransactionCallback<Object>()
                {
                    @Override
                    public Object doInTransaction()
                    {
                        auditLogService.truncateLog();
                        return null;
                    }
                });
            }
        }
    }
}
