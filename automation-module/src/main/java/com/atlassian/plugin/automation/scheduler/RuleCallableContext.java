package com.atlassian.plugin.automation.scheduler;

import com.atlassian.plugin.automation.config.RuleConfigStore;
import com.atlassian.plugin.automation.core.Rule;
import com.atlassian.plugin.automation.module.AutomationModuleManager;
import com.atlassian.plugin.automation.auditlog.EventAuditLogService;
import com.atlassian.sal.api.transaction.TransactionTemplate;

class RuleCallableContext
{
    private final Rule rule;
    private final ThreadLocalExecutor threadLocalExecutor;
    private final EventAuditLogService eventAuditLogService;
    private final TransactionTemplate transactionTemplate;
    private final AutomationModuleManager automationModuleManager;
    private final RuleExecutionLimiter ruleExecutionLimiter;
    private final RuleConfigStore ruleConfigStore;
    private final AutomationScheduler automationScheduler;
    private final SettingsAwareRuleLogService ruleLogService;

    RuleCallableContext(final Rule rule, final ThreadLocalExecutor threadLocalExecutor, final EventAuditLogService eventAuditLogService, final TransactionTemplate transactionTemplate,
                        final AutomationModuleManager automationModuleManager, final RuleExecutionLimiter ruleExecutionLimiter, final RuleConfigStore ruleConfigStore,
                        final AutomationScheduler automationScheduler, final SettingsAwareRuleLogService ruleLogService)
    {
        this.rule = rule;
        this.threadLocalExecutor = threadLocalExecutor;
        this.eventAuditLogService = eventAuditLogService;
        this.transactionTemplate = transactionTemplate;
        this.automationModuleManager = automationModuleManager;
        this.ruleExecutionLimiter = ruleExecutionLimiter;
        this.ruleConfigStore = ruleConfigStore;
        this.automationScheduler = automationScheduler;
        this.ruleLogService = ruleLogService;
    }

    AutomationScheduler getAutomationScheduler()
    {
        return automationScheduler;
    }

    RuleConfigStore getRuleConfigStore()
    {
        return ruleConfigStore;
    }

    Rule getRule()
    {
        return rule;
    }

    ThreadLocalExecutor getThreadLocalExecutor()
    {
        return threadLocalExecutor;
    }

    EventAuditLogService getEventAuditLogService()
    {
        return eventAuditLogService;
    }

    TransactionTemplate getTransactionTemplate()
    {
        return transactionTemplate;
    }

    AutomationModuleManager getAutomationModuleManager()
    {
        return automationModuleManager;
    }

    RuleExecutionLimiter getRuleExecutionLimiter()
    {
        return ruleExecutionLimiter;
    }

    SettingsAwareRuleLogService getSettingsAwareRuleLogService()
    {
        return ruleLogService;
    }
}
