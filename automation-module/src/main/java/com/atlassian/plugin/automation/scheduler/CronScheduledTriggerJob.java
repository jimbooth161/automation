package com.atlassian.plugin.automation.scheduler;

import com.atlassian.plugin.automation.core.Rule;
import com.atlassian.plugin.automation.core.TriggerContext;
import com.atlassian.plugin.automation.spi.scheduler.CronScheduler;
import com.atlassian.sal.api.scheduling.PluginJob;
import com.atlassian.sal.api.transaction.TransactionCallback;
import org.apache.log4j.Logger;

import java.util.Map;

public class CronScheduledTriggerJob implements PluginJob
{
    private static final Logger log = Logger.getLogger(CronScheduledTriggerJob.class);

    @Override
    public void execute(final Map<String, Object> params)
    {
        final RuleCallableContext ruleContext = (RuleCallableContext) params.get(CronScheduler.JOB_KEY_RULE_CONTEXT);

        try
        {
            ruleContext.getTransactionTemplate().execute(new TransactionCallback<Object>()
            {
                @Override
                public Object doInTransaction()
                {
                    final Rule rule = ruleContext.getRule();
                    ruleContext.getThreadLocalExecutor().executeAs(
                            rule.getActor(),
                            new RuleCallable(ruleContext, new TriggerContext(rule.getActor())),
                            false);

                    return null;
                }
            });
        }
        catch (RuntimeException e)
        {
            log.error("Unexpected error executing automation rule '" + ruleContext.getRule().getName() + "'", e);
        }
    }
}
