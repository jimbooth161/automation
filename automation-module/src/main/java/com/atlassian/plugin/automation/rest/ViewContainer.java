package com.atlassian.plugin.automation.rest;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Container for trigger or action view.
 */
public class ViewContainer
{
    @JsonProperty
    private int id;

    @JsonProperty
    private String moduleKey;

    @JsonProperty
    private String html;

    @JsonCreator
    public ViewContainer(@JsonProperty ("id") final int id, @JsonProperty ("moduleKey") String moduleKey, @JsonProperty ("html") final String html)
    {
        this.id = id;
        this.moduleKey = moduleKey;
        this.html = html;
    }

    public int getId()
    {
        return id;
    }

    public String getModuleKey()
    {
        return moduleKey;
    }

    public String getHtml()
    {
        return html;
    }
}
