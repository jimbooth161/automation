package com.atlassian.plugin.automation.scheduler;

import com.atlassian.plugin.automation.spi.scheduler.ThreadLocalContextProvider;
import com.atlassian.plugin.automation.spi.scheduler.UserAuthenticationContextService;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.concurrent.Callable;

/**
 * Runs a callable as a local task, using given actor
 */
@Named
public class ThreadLocalExecutor
{
    private static final Logger log = Logger.getLogger(ThreadLocalExecutor.class);
    private final ThreadLocalContextProvider threadLocalContextProvider;
    private final UserAuthenticationContextService userAuthenticationContextService;

    @Inject
    public ThreadLocalExecutor(@ComponentImport ThreadLocalContextProvider threadLocalContextProvider,
                               @ComponentImport UserAuthenticationContextService userAuthenticationContextService)
    {
        this.threadLocalContextProvider = threadLocalContextProvider;
        this.userAuthenticationContextService = userAuthenticationContextService;
    }

    /**
     * Execute the callable as the given actor.
     *
     * @param actor The user name of the actor
     * @param callable The task to execute
     * @param prepareThread Specify if the environment needs to be setup on the thread. If you execute the task
     *                      on a JIRA thread (e.g. a request thread), then this should be false because the environment
     *                      is already prepared. If you execute the task on a new thread (e.g. a thread pool), then
     *                      this should be true so that the environment is setup correctly.
     */
    public void executeAs(String actor, Callable<ErrorCollection> callable, boolean prepareThread)
    {
        // Switch to new user
        String originalActor = userAuthenticationContextService.getCurrentUser();
        userAuthenticationContextService.setCurrentUser(actor);

        // Optionally setup the thread
        if (prepareThread)
        {
            threadLocalContextProvider.preCall();
        }

        try
        {
            callable.call();
        }
        catch (Exception e)
        {
            log.error("Exception while running callable", e);
        }
        finally
        {
            // Optionally clean up the thread
            if (prepareThread)
            {
                threadLocalContextProvider.postCall();
            }

            // Switch back to original user
            userAuthenticationContextService.setCurrentUser(originalActor);
        }
    }
}
