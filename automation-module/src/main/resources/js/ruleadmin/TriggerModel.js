AJS.namespace("Atlassian.Automation.TriggerModel");

Atlassian.Automation.TriggerModel = Brace.Model.extend({
    namedAttributes: ["moduleKey", "params"],

    mixins: [Atlassian.Automation.Mixin.SetParams],

    defaults: {
        params:{}
    },

    initialize: function () {
    },

    parse: function (resp) {
        return {
            id: resp.id,
            moduleKey: resp.moduleKey,
            params: resp.params
        };
    }
});