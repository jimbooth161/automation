(function () {
    AJS.$(function () {
        var copying = false;
        var url = window.location.href;
        var editIndex = url.indexOf("/edit/");
        if (editIndex >= 0) {
            var ruleId = parseInt(url.substr(editIndex + "/edit/".length));
        } else {
            var copyIndex = url.indexOf("/copy/");
            if (copyIndex >= 0) {
                var ruleId = parseInt(url.substr(copyIndex + "/copy/".length));
                copying = true;
            }
        }

        AJS.$.ajax({
            url: AJS.contextPath() + "/rest/automation/1.0/config/editRuleConfig",
            contentType: "application/json",
            success: function (resp) {
                var ruleProperties = {
                    currentUser: resp.currentUser,
                    triggerModules: resp.triggers,
                    actionModules: resp.actions,
                    copiedRule: copying
                };

                var ruleModel;
                if (typeof ruleId !== 'undefined' && !isNaN(ruleId)) {
                    //we're editing!
                    ruleModel = new Atlassian.Automation.RuleModel({id: ruleId});
                    ruleProperties.rule = ruleModel;
                }

                var ruleConfigModel = new Atlassian.Automation.RuleConfigModel(ruleProperties);
                var ruleConfigView = new Atlassian.Automation.RuleConfigView({
                    model: ruleConfigModel,
                    el: AJS.$("#rule-config-container")
                });

                //if we're editing fetch the model from the server
                if (ruleModel) {
                    ruleModel.fetch({
                        success:function() {
                            if (copying) {
                                ruleModel.set("name", "Copy of " + ruleModel.get("name"));
                            }
                            ruleConfigView.render();
                        },
                        error: function() {
                            alert(AJS.format(AJS.I18n.getText("automation.plugin.error.loading.rule"), ruleId));
                            window.location.href = AJS.contextPath() + "/plugins/servlet/automationrules";
                        }
                    });
                } else {
                    ruleConfigView.render();
                }
            }
        });
    });
})();