AJS.namespace("Atlassian.Automation.RuleConfigView");

Atlassian.Automation.RuleConfigView = Brace.View.extend({
    template: Atlassian.Templates.Automation.ruleConfigContainer,

    events: {
        "submit #rule-config-form":"nextStep",
        "click #progress-rule-form": "nextStep"
    },

    initialize: function () {
        _.bindAll(this, 'render', 'renderForm', 'nextStep');
        this.progressIndicatorView = new Atlassian.Automation.ProgressIndicatorView({
            model: this.model
        });

        this.model.on("change:progress", this.renderForm);
    },

    render: function () {
        this.$el.html(this.template({
            isEditing: !this.model.get("rule").isNew()
        }));

        var $progressIndicator = this.progressIndicatorView.render().$el;
        this.$(".aui-page-header-actions").html($progressIndicator);

        this.formContainer = this.$("#form-container");
        this.renderForm();

        return this;
    },

    renderForm: function() {
        if(this.formView) {
            this.formView.destroy();
        }

        switch (this.model.get("progress")) {
            case 0:
                this.formView = new Atlassian.Automation.RuleFormView({
                    model: this.model,
                    el: this.formContainer
                });
                break;
            case 1:
                this.formView = new Atlassian.Automation.TriggerFormView({
                    model: this.model,
                    el: this.formContainer
                });
                break;
            case 2:
                this.formView = new Atlassian.Automation.ActionFormView({
                    model: this.model,
                    el: this.formContainer
                });
                break;
            case 3:
                this.formView = new Atlassian.Automation.RuleConfirmView({
                    model: this.model,
                    el: this.formContainer
                });
                break;
        }

        this.formView.render();
    },

    nextStep: function(e) {
        e.preventDefault();

        this.model.trigger("saveFormState");

        var $throbber = this.$(".buttons-container .throbber");
        $throbber.addClass("loading");
        this.model.validateAndIncrementProgress(function() {
            $throbber.removeClass("loading");
        });
    }
});