package com.atlassian.plugin.automation.jira.action;

import com.atlassian.fugue.Either;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.bc.issue.comment.CommentService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueInputParameters;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.plugin.automation.config.DefaultAutomationConfiguration;
import com.atlassian.plugin.automation.jira.util.SecurityLevelContextProvider;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.internal.verification.Times;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.anyMap;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestCommentIssueAction
{
    @Mock
    private UserManager userManager;
    @Mock
    private IssueService issueService;
    @Mock
    private SoyTemplateRenderer soyTemplateRenderer;
    @Mock
    private TemplateRenderer templateRenderer;
    @Mock
    private I18nResolver i18nResolver;
    @Mock
    private CommentService commentService;
    @Mock
    private SecurityLevelContextProvider securityLevelContextProvider;
    @Mock
    private MutableIssue issue;

    private CommentIssueAction commentIssueAction;

    @Before
    public void setUp() throws Exception
    {
        when(i18nResolver.getText(anyString())).thenReturn("translated text");
        when(templateRenderer.renderFragment(anyString(), anyMap())).thenAnswer(new Answer<Object>()
        {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable
            {
                return "rendered " + invocationOnMock.getArguments()[0];
            }
        });

        commentIssueAction = new CommentIssueAction(commentService, userManager, issueService, soyTemplateRenderer, templateRenderer, securityLevelContextProvider);
        when(issue.getId()).thenReturn(1000l);
        IssueService.IssueResult issueResult = new IssueService.IssueResult(issue);
        when(issueService.getIssue(any(ApplicationUser.class), anyLong())).thenReturn(issueResult);
    }

    @Test
    public void testExecutePasses() throws Exception
    {
        Map<String, List<String>> params = Maps.newHashMap();
        params.put(CommentIssueAction.COMMENT_KEY, Lists.newArrayList("Test"));
        commentIssueAction.init(new DefaultAutomationConfiguration(-1, "", params));

        List<Issue> issues = Lists.newArrayList((Issue) issue);

        ErrorCollection errorCollection = new ErrorCollection();
        commentIssueAction.execute("actor", issues, errorCollection);

        verify(commentService, new Times(1)).create(any(ApplicationUser.class),
                same(issue), eq("rendered Test"), (String) same(null), (Long) eq(null), eq(false), any(com.atlassian.jira.util.ErrorCollection.class));
        assertFalse("errors present", errorCollection.hasAnyErrors());
    }

    @Test
    public void testExecuteFails() throws Exception
    {
        final Map<String, List<String>> params = Maps.newHashMap();
        params.put(CommentIssueAction.COMMENT_KEY, Lists.newArrayList("Test"));
        params.put(CommentIssueAction.COMMENT_SECURITY_KEY, Lists.newArrayList("role:1"));

        when(securityLevelContextProvider.extractValue("role:1")).thenReturn(Either.<String, Long>right(1L));

        commentIssueAction.init(new DefaultAutomationConfiguration(-1, "", params));

        List<Issue> issues = Lists.newArrayList((Issue) issue);
        ErrorCollection errorCollection = new ErrorCollection();

        // Setup the comment service mock to return error
        when(commentService.create(any(ApplicationUser.class),
                same(issue), eq("rendered Test"), (String) same(null), eq(1l), eq(false), any(com.atlassian.jira.util.ErrorCollection.class))).
                then(new Answer<Object>()
                {
                    @Override
                    public Object answer(InvocationOnMock invocationOnMock) throws Throwable
                    {
                        final com.atlassian.jira.util.ErrorCollection jiraErrorCollection = (com.atlassian.jira.util.ErrorCollection)
                                invocationOnMock.getArguments()[invocationOnMock.getArguments().length - 1];
                        jiraErrorCollection.addErrorMessage("error occurred");
                        return null;
                    }
                });
        IssueService.TransitionValidationResult validationResult = mock(IssueService.TransitionValidationResult.class);
        when(issueService.validateTransition(any(ApplicationUser.class), eq(1000l), eq(10), any(IssueInputParameters.class))).
                thenReturn(validationResult);

        commentIssueAction.execute("actor", issues, errorCollection);

        assertTrue("no errors present", errorCollection.hasAnyErrors());
    }

    @Test
    public void testValidateAddConfiguration() throws Exception
    {
        Map<String, List<String>> params = Maps.newHashMap();
        assertTrue("no error produced", commentIssueAction.validateAddConfiguration(i18nResolver, params, "").hasAnyErrors());

        params.put(CommentIssueAction.COMMENT_KEY, Lists.newArrayList(""));
        assertTrue("no error produced", commentIssueAction.validateAddConfiguration(i18nResolver, params, "").hasAnyErrors());

        params.put(CommentIssueAction.COMMENT_KEY, Lists.newArrayList("Test comment"));
        assertFalse("error produced", commentIssueAction.validateAddConfiguration(i18nResolver, params, "").hasAnyErrors());

        final ErrorCollection errors = new ErrorCollection();
        errors.addErrorMessage("Invalid security level!");
        when(securityLevelContextProvider.validate(Matchers.<I18nResolver>any(), Matchers.<ApplicationUser>any(), Matchers.<Project>any(), anyString())).thenReturn(errors);
        params.put(CommentIssueAction.COMMENT_SECURITY_KEY, Lists.newArrayList("role:a"));
        assertTrue("no error produced", commentIssueAction.validateAddConfiguration(i18nResolver, params, "").hasAnyErrors());

        when(securityLevelContextProvider.validate(Matchers.<I18nResolver>any(), Matchers.<ApplicationUser>any(), Matchers.<Project>any(), anyString())).thenReturn(new ErrorCollection());
        params.put(CommentIssueAction.COMMENT_SECURITY_KEY, Lists.newArrayList("role:1"));
        assertFalse("error produced", commentIssueAction.validateAddConfiguration(i18nResolver, params, "").hasAnyErrors());
    }
}
