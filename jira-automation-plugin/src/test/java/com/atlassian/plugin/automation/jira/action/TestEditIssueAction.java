package com.atlassian.plugin.automation.jira.action;

import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueInputParameters;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.plugin.automation.config.DefaultAutomationConfiguration;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.internal.verification.Times;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.anyMap;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestEditIssueAction
{
    @Mock
    private UserManager userManager;
    @Mock
    private IssueService issueService;
    @Mock
    private SoyTemplateRenderer soyTemplateRenderer;
    @Mock
    private TemplateRenderer templateRenderer;
    @Mock
    private I18nResolver i18nResolver;
    @Mock
    private WorkflowManager workflowManager;
    @Mock
    private CustomFieldManager customFieldManager;
    @Mock
    private MutableIssue issue;

    @Before
    public void setUp() throws Exception
    {
        when(i18nResolver.getText(Matchers.anyString())).thenReturn("translated text");
        when(templateRenderer.renderFragment(anyString(), anyMap())).thenAnswer(new Answer<Object>()
        {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable
            {
                String value = (String) invocationOnMock.getArguments()[0];
                return new StringBuilder(value).reverse().toString();
            }
        });
        when(issue.getId()).thenReturn(1000l);
        IssueService.IssueResult issueResult = new IssueService.IssueResult(issue);
        when(issueService.getIssue(any(ApplicationUser.class), anyLong())).thenReturn(issueResult);
    }

    @Test
    public void testExecutePasses() throws Exception
    {
        EditIssueAction editIssueAction = new EditIssueAction(userManager, issueService, soyTemplateRenderer, templateRenderer, customFieldManager);
        Map<String, List<String>> params = Maps.newHashMap();
        params.put(EditIssueAction.EDIT_FIELDS_KEY, Lists.newArrayList("resolution=3\npriority=2\nsummary=SUMMARY $issue.summary"));
        params.put(EditIssueAction.ALLOW_VARIABLE_EXPANSION_KEY, Lists.newArrayList("true"));
        editIssueAction.init(new DefaultAutomationConfiguration(-1, "", params));

        when(issue.getSummary()).thenReturn("The Summary");
        List<Issue> issues = Lists.newArrayList((Issue) issue);
        ErrorCollection errorCollection = new ErrorCollection();

        final IssueService.UpdateValidationResult validationResult = mock(IssueService.UpdateValidationResult.class);
        when(issueService.validateUpdate(Matchers.any(ApplicationUser.class), Matchers.eq(1000l), Matchers.any(IssueInputParameters.class))).then(new Answer<Object>()
        {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable
            {
                IssueInputParameters issueInputParameters = (IssueInputParameters) invocationOnMock.getArguments()[2];
                assertEquals("invalid resolution ID", "3", issueInputParameters.getResolutionId());
                assertEquals("invalid priority ID", "2", issueInputParameters.getPriorityId());
                assertEquals("invalid summary", "yrammus.eussi$ YRAMMUS", issueInputParameters.getSummary());
                return validationResult;
            }
        });
        when(validationResult.isValid()).thenReturn(true);

        IssueService.IssueResult updateResult = mock(IssueService.IssueResult.class);
        when(updateResult.isValid()).thenReturn(true);
        when(issueService.update(Matchers.any(ApplicationUser.class), Matchers.any(IssueService.UpdateValidationResult.class), eq(EventDispatchOption.ISSUE_UPDATED), eq(false))).
                thenReturn(updateResult);

        editIssueAction.execute("actor", issues, errorCollection);

        verify(issueService).update(Matchers.any(ApplicationUser.class), Matchers.same(validationResult), Matchers.same(EventDispatchOption.ISSUE_UPDATED), Matchers.eq(false));
        assertFalse("errors present", errorCollection.hasAnyErrors());
    }

    @Test
    public void testExecuteFails() throws Exception
    {
        EditIssueAction editIssueAction = new EditIssueAction(userManager, issueService, soyTemplateRenderer, templateRenderer, customFieldManager);
        Map<String, List<String>> params = Maps.newHashMap();
        editIssueAction.init(new DefaultAutomationConfiguration(-1, "", params));

        List<Issue> issues = Lists.newArrayList((Issue) issue);
        ErrorCollection errorCollection = new ErrorCollection();

        IssueService.UpdateValidationResult validationResult = mock(IssueService.UpdateValidationResult.class);
        when(issueService.validateUpdate(Matchers.any(ApplicationUser.class), Matchers.eq(1000l), Matchers.any(IssueInputParameters.class))).
                thenReturn(validationResult);

        when(validationResult.isValid()).thenReturn(false);
        final SimpleErrorCollection validationErrorCollection = new SimpleErrorCollection();
        validationErrorCollection.addErrorMessage("error");

        when(validationResult.getErrorCollection()).thenReturn(validationErrorCollection);

        editIssueAction.execute("actor", issues, errorCollection);

        verify(issueService, new Times(0)).update(Matchers.any(ApplicationUser.class), Matchers.same(validationResult));
        assertTrue("no errors present", errorCollection.hasAnyErrors());
    }

    @Test
    public void testExecuteFailsValidator() throws Exception
    {
        EditIssueAction editIssueAction = new EditIssueAction(userManager, issueService, soyTemplateRenderer, templateRenderer, customFieldManager);
        Map<String, List<String>> params = Maps.newHashMap();
        editIssueAction.init(new DefaultAutomationConfiguration(-1, "", params));

        List<Issue> issues = Lists.newArrayList((Issue) issue);
        ErrorCollection errorCollection = new ErrorCollection();

        IssueService.UpdateValidationResult validationResult = mock(IssueService.UpdateValidationResult.class);
        when(issueService.validateUpdate(Matchers.any(ApplicationUser.class), Matchers.eq(1000l), Matchers.any(IssueInputParameters.class))).
                thenReturn(validationResult);

        when(validationResult.isValid()).thenReturn(true);

        IssueService.IssueResult updateResult = mock(IssueService.IssueResult.class);
        final SimpleErrorCollection updateErrorCollection = new SimpleErrorCollection();
        updateErrorCollection.addErrorMessage("error");
        when(updateResult.isValid()).thenReturn(false);
        when(updateResult.getErrorCollection()).thenReturn(updateErrorCollection);

        when(issueService.update(Matchers.any(ApplicationUser.class), Matchers.any(IssueService.UpdateValidationResult.class), eq(EventDispatchOption.ISSUE_UPDATED), eq(false))).
                thenReturn(updateResult);

        editIssueAction.execute("actor", issues, errorCollection);

        verify(issueService, new Times(0)).update(Matchers.any(ApplicationUser.class), Matchers.same(validationResult));
        assertTrue("no errors present", errorCollection.hasAnyErrors());
    }

    @Test
    public void testValidateAddConfigurationFieldsConfig() throws Exception
    {
        EditIssueAction editIssueAction = new EditIssueAction(userManager, issueService, soyTemplateRenderer, templateRenderer, customFieldManager);
        Map<String, List<String>> params = Maps.newHashMap();
        params.put(EditIssueAction.EDIT_FIELDS_KEY, Lists.newArrayList(""));
        assertTrue("error produced", editIssueAction.validateAddConfiguration(i18nResolver, params, "").hasAnyErrors());

        params.put(EditIssueAction.EDIT_FIELDS_KEY, Lists.newArrayList("a"));
        assertTrue("no error produced", editIssueAction.validateAddConfiguration(i18nResolver, params, "").hasAnyErrors());

        params.put(EditIssueAction.EDIT_FIELDS_KEY, Lists.newArrayList("a="));
        assertFalse("error produced", editIssueAction.validateAddConfiguration(i18nResolver, params, "").hasAnyErrors());

        params.put(EditIssueAction.EDIT_FIELDS_KEY, Lists.newArrayList("a=10"));
        assertFalse("error produced", editIssueAction.validateAddConfiguration(i18nResolver, params, "").hasAnyErrors());
    }
}
