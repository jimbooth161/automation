package com.atlassian.plugin.automation.jira.action;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.label.Label;
import com.atlassian.jira.issue.label.LabelManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.plugin.automation.config.DefaultAutomationConfiguration;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestEditLabelsAction extends TestCase
{
    @Mock
    private UserManager userManager;
    @Mock
    private LabelManager labelManager;
    @Mock
    private SoyTemplateRenderer soyTemplateRenderer;
    @Mock
    private I18nResolver i18nResolver;
    @Mock
    private WorkflowManager workflowManager;

    @Before
    public void setUp() throws Exception
    {
        when(i18nResolver.getText(anyString())).thenReturn("translated text");
    }

    @Test
    public void testExecutePasses()
    {
        EditLabelsAction editLabelsAction = new EditLabelsAction(userManager, soyTemplateRenderer, labelManager);
        Map<String, List<String>> params = Maps.newHashMap();
        params.put(EditLabelsAction.EDIT_LABELS_ADD_KEY, Lists.newArrayList("label1", "label2"));
        params.put(EditLabelsAction.EDIT_LABELS_REMOVE_KEY, Lists.newArrayList("label3", "label4"));
        params.put(EditLabelsAction.EDIT_LABLES_NOTIFICATION_KEY, Lists.newArrayList("true"));

        assertFalse("shouldn't fail validation", editLabelsAction.validateAddConfiguration(i18nResolver, params, "actor").hasAnyErrors());
        editLabelsAction.init(new DefaultAutomationConfiguration(-1, "", params));

        Issue issue = mock(MutableIssue.class);
        when(issue.getId()).thenReturn(1000l);
        List<Issue> issues = Lists.newArrayList(issue);
        ErrorCollection errorCollection = new ErrorCollection();

        when(labelManager.getLabels(issue.getId())).thenReturn(Sets.<Label>newHashSet());
        editLabelsAction.execute("actor", issues, errorCollection);

        verify(labelManager, atMost(1)).
                setLabels(any(ApplicationUser.class), eq(1000l), eq(Sets.newHashSet("label1", "label2")), eq(true), eq(true));
        assertFalse("errors present", errorCollection.hasAnyErrors());
    }

    @SuppressWarnings("Duplicates") @Test
    public void testExecuteRemoveExistingCaseInsensitive()
    {
        EditLabelsAction editLabelsAction = new EditLabelsAction(userManager, soyTemplateRenderer, labelManager);
        Map<String, List<String>> params = Maps.newHashMap();
        params.put(EditLabelsAction.EDIT_LABELS_ADD_KEY, Lists.newArrayList("label1", "label2"));
        params.put(EditLabelsAction.EDIT_LABELS_REMOVE_KEY, Lists.newArrayList("label3", "label4"));
        params.put(EditLabelsAction.EDIT_LABLES_CASEINSENSITIVE_KEY, Lists.newArrayList("true"));

        assertFalse("shouldn't fail validation", editLabelsAction.validateAddConfiguration(i18nResolver, params, "actor").hasAnyErrors());
        editLabelsAction.init(new DefaultAutomationConfiguration(-1, "", params));

        Issue issue = mock(MutableIssue.class);
        when(issue.getId()).thenReturn(1000l);
        List<Issue> issues = Lists.newArrayList(issue);
        ErrorCollection errorCollection = new ErrorCollection();

        Label label2 = new Label(1l, 1000l, "label2");
        Label label3 = new Label(2l, 1000l, "label3");
        Label label31 = new Label(3l, 1000l, "LABEL3");
        Label label32 = new Label(4l, 1000l, "LabeL3");

        when(labelManager.getLabels(issue.getId())).thenReturn(Sets.newHashSet(label2, label3, label31, label32));
        editLabelsAction.execute("actor", issues, errorCollection);

        verify(labelManager, atMost(1)).
                setLabels(any(ApplicationUser.class), eq(1000l), eq(Sets.newHashSet("label1")), eq(false), eq(true));
        assertFalse("errors present", errorCollection.hasAnyErrors());
    }

    @SuppressWarnings("Duplicates")
    @Test
    public void testExecuteRemoveExistingCaseSensitive()
    {
        EditLabelsAction editLabelsAction = new EditLabelsAction(userManager, soyTemplateRenderer, labelManager);
        Map<String, List<String>> params = Maps.newHashMap();
        params.put(EditLabelsAction.EDIT_LABELS_ADD_KEY, Lists.newArrayList("label1", "label2"));
        params.put(EditLabelsAction.EDIT_LABELS_REMOVE_KEY, Lists.newArrayList("label3", "label4"));
        params.put(EditLabelsAction.EDIT_LABLES_CASEINSENSITIVE_KEY, Lists.newArrayList("false"));

        assertFalse("shouldn't fail validation", editLabelsAction.validateAddConfiguration(i18nResolver, params, "actor").hasAnyErrors());
        editLabelsAction.init(new DefaultAutomationConfiguration(-1, "", params));

        Issue issue = mock(MutableIssue.class);
        when(issue.getId()).thenReturn(1000l);
        List<Issue> issues = Lists.newArrayList(issue);
        ErrorCollection errorCollection = new ErrorCollection();

        Label label2 = new Label(1l, 1000l, "label2");
        Label label3 = new Label(2l, 1000l, "label3");
        Label label31 = new Label(3l, 1000l, "LABEL3");
        Label label32 = new Label(4l, 1000l, "LabeL3");

        when(labelManager.getLabels(issue.getId())).thenReturn(Sets.newHashSet(label2, label3, label31, label32));
        editLabelsAction.execute("actor", issues, errorCollection);

        verify(labelManager, times(1)).
                setLabels(any(ApplicationUser.class), eq(1000l), eq(Sets.newHashSet("label1", "label2", "LABEL3", "LabeL3")), eq(false), eq(true));
        assertFalse("errors present", errorCollection.hasAnyErrors());
    }
    
    @Test
    public void testValidateFails()
    {
        EditLabelsAction editLabelsAction = new EditLabelsAction(userManager, soyTemplateRenderer, labelManager);
        Map<String, List<String>> params = Maps.newHashMap();

        assertTrue("should fail validation", editLabelsAction.validateAddConfiguration(i18nResolver, params, "actor").hasAnyErrors());

        params.put(EditLabelsAction.EDIT_LABELS_ADD_KEY, Lists.newArrayList(""));
        params.put(EditLabelsAction.EDIT_LABELS_REMOVE_KEY, Lists.newArrayList(""));

        assertTrue("should fail validation (1)", editLabelsAction.validateAddConfiguration(i18nResolver, params, "actor").hasAnyErrors());

        params.put(EditLabelsAction.EDIT_LABELS_ADD_KEY, Lists.newArrayList("", ""));
        params.put(EditLabelsAction.EDIT_LABELS_REMOVE_KEY, Lists.newArrayList("", ""));
        assertTrue("should fail validation (2)", editLabelsAction.validateAddConfiguration(i18nResolver, params, "actor").hasAnyErrors());
    }
}
