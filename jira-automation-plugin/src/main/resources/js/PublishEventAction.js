(function() {
    AJS.$(AJS).bind("AJS.automation.form.loaded", function(e, context) {

        new AJS.SingleSelect({
            element:AJS.$("#jiraActionEventId", context),
            itemAttrDisplayed: "label"
        });
    });
})();