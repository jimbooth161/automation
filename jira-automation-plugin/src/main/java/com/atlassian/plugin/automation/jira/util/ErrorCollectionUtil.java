package com.atlassian.plugin.automation.jira.util;

import com.atlassian.plugin.automation.util.ErrorCollection;

import java.util.Map;

public class ErrorCollectionUtil
{
    /**
     * Translate JIRA error collection to ATM error collection
     * @param jiraErrorCollection
     * @return
     */
    public static ErrorCollection transform(com.atlassian.jira.util.ErrorCollection jiraErrorCollection)
    {
        final ErrorCollection result = new ErrorCollection();
        for (Map.Entry<String,String> entry : jiraErrorCollection.getErrors().entrySet())
        {
            result.addError(entry.getKey(), entry.getValue());
        }

        for (String errorMessage : jiraErrorCollection.getErrorMessages())
        {
            result.addErrorMessage(errorMessage);
        }
        return result;
    }
}
