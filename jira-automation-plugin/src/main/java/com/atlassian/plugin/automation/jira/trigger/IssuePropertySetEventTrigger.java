package com.atlassian.plugin.automation.jira.trigger;

import com.atlassian.jira.event.issue.property.IssuePropertySetEvent;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.plugin.automation.core.EventTrigger;
import com.atlassian.plugin.automation.core.TriggerContext;
import com.atlassian.plugin.automation.core.auditlog.AuditString;
import com.atlassian.plugin.automation.core.auditlog.DefaultAuditString;
import com.atlassian.plugin.automation.core.trigger.TriggerConfiguration;
import com.atlassian.plugin.automation.jira.util.Constants;
import com.atlassian.plugin.automation.jira.util.JqlMatcherService;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.plugin.automation.util.ParameterUtil;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.atlassian.plugin.automation.jira.util.Constants.CONFIG_COMPLETE_KEY;
import static com.atlassian.plugin.automation.util.ParameterUtil.singleValue;

@Scanned
public class IssuePropertySetEventTrigger implements EventTrigger<Issue>
{
    private static final Logger log = LoggerFactory.getLogger(IssuePropertySetEventTrigger.class);

    private static final String RESOURCE_KEY = "com.atlassian.plugin.automation.jira-automation-plugin:jira-config-resources";

    public static final String EVENT_PROPERTY_KEYS = "eventPropertyKeys";
    public static final String EVENT_JQL_KEY = "jiraJqlExpression";

    private final SoyTemplateRenderer soyTemplateRenderer;
    private List<String> propertyKeys;
    private final JqlMatcherService jqlMatcherService;

    private String jql;
    private boolean supportsIssuePropertySetEvent;

    @Inject
    public IssuePropertySetEventTrigger(
            @ComponentImport final SoyTemplateRenderer soyTemplateRenderer,
            @ComponentImport final BuildUtilsInfo buildUtilsInfo,
            final JqlMatcherService jqlMatcherService)
    {
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.jqlMatcherService = jqlMatcherService;

        // JIRA 6.2 introduced IssuePropertySetEvent - this trigger will not work for versions before that
        this.supportsIssuePropertySetEvent = buildUtilsInfo.getApplicationBuildNumber() >= Constants.JIRA_62_BUILD_NUMBER;
    }

    @Override
    public void init(TriggerConfiguration config)
    {
        jql = singleValue(config, EVENT_JQL_KEY);
        final String eventPropertyKeys = singleValue(config, EVENT_PROPERTY_KEYS);
        if (StringUtils.isNotBlank(eventPropertyKeys))
        {
            propertyKeys = Arrays.asList(eventPropertyKeys.trim().split("\\s*,\\s*"));
        }
    }

    @Override
    public Iterable<Issue> getItems(TriggerContext context, ErrorCollection errorCollection)
    {
        if (supportsIssuePropertySetEvent)
        {
            final IssuePropertySetEvent issuePropertySetEvent = (IssuePropertySetEvent) context.getSourceEvent();
            if (issuePropertySetEvent != null)
            {
                if (propertyKeys.contains(issuePropertySetEvent.getEntityProperty().getKey()))
                {
                    final Issue issue = jqlMatcherService.getMatchedIssue(context, errorCollection, jql, issuePropertySetEvent.getEntityProperty().getEntityId());
                    if (issue != null)
                    {
                        return Collections.singleton(issue);
                    }
                }
            }
        }
        return Collections.emptyList();
    }

    @Override
    public AuditString getAuditLog()
    {
        return new DefaultAuditString(String.format("Issue Property Set Event trigger for property keys %s", Arrays.toString(propertyKeys.toArray())));
    }

    @Override
    public String getConfigurationTemplate(TriggerConfiguration triggerConfiguration, final String actor)
    {
        try
        {
            final Map<String, Object> context = Maps.newHashMap();
            ParameterUtil.transformParams(context, triggerConfiguration);

            return soyTemplateRenderer.render(RESOURCE_KEY, "Atlassian.Templates.Automation.JIRA.issuePropertySetEvent", context);
        }
        catch (SoyException e)
        {
            log.error("Error rendering template", e);
            return "Unable to render configuration form. Consult your server logs or administrator.";
        }
    }

    @Override
    public String getViewTemplate(final TriggerConfiguration config, final String actor)
    {
        try
        {
            final Map<String, Object> context = Maps.newHashMap();
            ParameterUtil.transformParams(context, config);

            return soyTemplateRenderer.render(CONFIG_COMPLETE_KEY, "Atlassian.Templates.Automation.JIRA.issuePropertySetEventView", context);
        }
        catch (SoyException e)
        {
            log.error("Error rendering template", e);
            return "Unable to render configuration form. Consult your server logs or administrator.";
        }
    }

    @Override
    public ErrorCollection validateAddConfiguration(I18nResolver i18n, Map<String, List<String>> params, final String actor)
    {
        final ErrorCollection errorCollection = new ErrorCollection();

        final String eventPropertyKeys = singleValue(params, EVENT_PROPERTY_KEYS);
        if (StringUtils.isBlank(eventPropertyKeys))
        {
            errorCollection.addError(EVENT_PROPERTY_KEYS, i18n.getText("automation.jira.issuepropertyset.keys.required"));
        }

        final String jqlString = singleValue(params, EVENT_JQL_KEY);
        if (StringUtils.isNotBlank(jqlString))
        {
            jqlMatcherService.validateJql(actor, EVENT_JQL_KEY, jqlString);
        }

        return errorCollection;
    }

    @Override
    public String getEventClassName()
    {
        return (supportsIssuePropertySetEvent ? "com.atlassian.jira.event.issue.property.IssuePropertySetEvent" : "");
    }
}


