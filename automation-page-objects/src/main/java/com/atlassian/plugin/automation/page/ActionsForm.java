package com.atlassian.plugin.automation.page;

import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.SelectElement;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.atlassian.plugin.automation.page.util.DialogUtils;
import com.atlassian.plugin.automation.page.util.RuleFormUtils;
import com.atlassian.webdriver.AtlassianWebDriver;
import org.openqa.selenium.By;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

public class ActionsForm extends AbstractConfigForm
{
    @Inject
    private AtlassianWebDriver driver;

    @Inject
    private FormFactory formFactory;

    @ElementBy(className = "remote-params")
    private PageElement remoteParams;

    @ElementBy(id = "rule-trigger-module-key")
    private SelectElement triggerSelectElement;

    @ElementBy(id = "add-action-button")
    private PageElement addActionLink;

    private int actionCount = 0;

    @WaitUntil
    public void isReady()
    {
        waitUntilTrue(remoteParams.timed().isPresent());
        waitUntilFalse(remoteParams.timed().hasClass("loading-action"));
    }

    public <T> T setInitialAction(Class<T> clazz)
    {
        final PageElement childForm = elementFinder.find(By.id("action-form-0"), TimeoutType.DEFAULT);
        return formFactory.build(clazz, childForm);
    }

    public <T> T getInitialAction(Class<T> clazz)
    {
        final PageElement childForm = elementFinder.find(By.id("action-form-0"), TimeoutType.DEFAULT);
        return formFactory.build(clazz, childForm);
    }

    public <T> T addAction(Class<T> clazz)
    {
        addActionLink.click();
        final PageElement childForm = elementFinder.find(By.id("action-form-" + (++actionCount)), TimeoutType.DEFAULT);
        return formFactory.build(clazz, childForm);
    }

    /**
     * Removes action with given index
     * @param actionIndex
     * @return
     */
    public ActionsForm removeAction(int actionIndex)
    {
        elementFinder.find(By.id("action-form-"+ actionIndex)).find(By.className("delete-action-button")).click();
        DialogUtils.acceptDialog(driver);

        return this;
    }

    public ConfirmationForm next()
    {
        RuleFormUtils.waitForFormRerender();
        nextButton.click();
        return pageBinder.bind(ConfirmationForm.class);
    }
}
