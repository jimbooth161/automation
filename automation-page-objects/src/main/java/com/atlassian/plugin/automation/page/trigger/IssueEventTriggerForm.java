package com.atlassian.plugin.automation.page.trigger;

import com.atlassian.jira.pageobjects.components.fields.MultiSelect;
import com.atlassian.jira.pageobjects.components.fields.SingleSelect;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.plugin.automation.page.ModuleKey;
import org.openqa.selenium.By;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * Represents an issue even trigger.
 */
@ModuleKey ("com.atlassian.plugin.automation.jira-automation-plugin:issue-event-trigger")
public class IssueEventTriggerForm extends TriggerForm
{
    /**
     * Module key might be null if no trigger should be set
     */
    public IssueEventTriggerForm(final PageElement container, final String moduleKey)
    {
        super(container, moduleKey);
    }

    public IssueEventTriggerForm setEvents(String... eventNames)
    {
        final PageElement element = container.find(By.name("jiraEventId"));
        waitUntilTrue(element.timed().isPresent());
        final MultiSelect eventMultiSelect = pageBinder.bind(MultiSelect.class, "jiraEventId");
        eventMultiSelect.clearAllItems();
        for (String eventName : eventNames)
        {
            eventMultiSelect.add(eventName);
        }
        return this;
    }

    public IssueEventTriggerForm setJql(String jql)
    {
        setTriggerParam("jiraJqlExpression", jql);
        return this;
    }

    public IssueEventTriggerForm toggleRestrictUsers()
    {
        container.find(By.id("restrictEventAuthors")).click();
        return this;
    }

    public IssueEventTriggerForm toggleAssigneeOnly()
    {
        container.find(By.id("enabledAssigneeRestrictions")).click();
        container.find(By.id("currentIsAssignee")).click();
        return this;
    }

    public IssueEventTriggerForm resetSynchronousProcessing()
    {
        return setSynchronousProcessing("none");
    }

    public IssueEventTriggerForm setEventProcessingSynchronous()
    {
        return setSynchronousProcessing("sync");
    }

    public IssueEventTriggerForm setEventProcessingAsynchronous()
    {
        return setSynchronousProcessing("async");
    }

    private IssueEventTriggerForm setSynchronousProcessing(final String value)
    {
        PageElement elementParent = container.find(By.id("sync-proc-div"));
        SingleSelect syncProcessingSelect = pageBinder.bind(SingleSelect.class, elementParent);
        syncProcessingSelect.select(value);
        return this;
    }
    
    public IssueEventTriggerForm setGroup(String groupName)
    {
        PageElement groupSelectParent = container.find(By.id("specific-group-field-group"));
        SingleSelect groupSingleSelect = pageBinder.bind(SingleSelect.class, groupSelectParent);
        groupSingleSelect.select(groupName);
        return this;
    }
}
