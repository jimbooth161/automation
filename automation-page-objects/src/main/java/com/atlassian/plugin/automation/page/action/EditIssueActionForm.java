package com.atlassian.plugin.automation.page.action;

import com.atlassian.pageobjects.elements.CheckboxElement;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.plugin.automation.page.ModuleKey;
import org.openqa.selenium.By;


@ModuleKey ("com.atlassian.plugin.automation.jira-automation-plugin:edit-issue-action")
public class EditIssueActionForm extends ActionForm
{
    public EditIssueActionForm(final PageElement container, final String moduleKey)
    {
        super(container, moduleKey);
    }

    public EditIssueActionForm setTransitionFields(final String fields)
    {
        setActionParam("jiraEditFields", fields);
        return this;
    }


    public EditIssueActionForm enableVariableExpansion(final boolean variableExpansionEnabled)
    {
        final CheckboxElement checkbox = container.find(By.name("jiraAllowVariableExpansion"), CheckboxElement.class);

        if (variableExpansionEnabled)
        {
            checkbox.check();
        }
        else
        {
            checkbox.uncheck();
        }

        return this;
    }

}
