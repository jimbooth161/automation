package com.atlassian.plugin.automation.page.trigger;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.plugin.automation.page.ModuleKey;


@ModuleKey ("com.atlassian.plugin.automation.jira-automation-plugin:jql-filter-trigger")
public class JqlFilterTriggerForm extends TriggerForm
{
    public JqlFilterTriggerForm(final PageElement container, final String moduleKey)
    {
        super(container, moduleKey);
    }

    public JqlFilterTriggerForm setCron(final String cron)
    {
        setTriggerParam("cronExpression", cron);
        return this;
    }

    public JqlFilterTriggerForm setJql(final String jql)
    {
        setTriggerParam("jiraJqlExpression", jql);
        return this;
    }

    public JqlFilterTriggerForm setLimitResults(final String limitResults)
    {
        setTriggerParam("jiraMaxResults", limitResults);
        return this;
    }
}
