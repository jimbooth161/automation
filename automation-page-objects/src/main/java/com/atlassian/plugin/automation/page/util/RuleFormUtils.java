package com.atlassian.plugin.automation.page.util;

public class RuleFormUtils
{
    public static void waitForFormRerender()
    {
        // this is pretty dummy now - we re-render the form in the browser, so sometimes Selenium is still hooked to an
        // old UI element (button etc). Following exception can occur:
        // org.openqa.selenium.StaleElementReferenceException: Element is no longer attached to the DOM Command duration or timeout: 33 milliseconds For documentation on this error, please visit: http://seleniumhq.org/exceptions/stale_element_reference.html Build info: version: '2.35.0', revision: '8df0c6bedf70ff9f22c647788f9fe9c8d22210e2', time: '2013-08-17 12:46:41' System info: os.name: 'Linux', os.arch: 'amd64', os.version: '3.2.0-69-virtual', java.version: '1.7.0_72' Session ID: f05c559e-708d-4dd2-8574-c32a2eb9ff16 Driver info: org.openqa.selenium.firefox.FirefoxDriver Capabilities [{platform=LINUX, databaseEnabled=true, cssSelectorsEnabled=true, javascriptEnabled=true, acceptSslCerts=true, handlesAlerts=true, browserName=firefox, browserConnectionEnabled=true, nativeEvents=false, webStorageEnabled=true, rotatable=false, locationContextEnabled=true, applicationCacheEnabled=true, takesScreenshot=true, version=23.0.1}]
        // Instead of refactoring all the ids to be very unique and properly waiting, putting this fix here.
        // TODO remove this if we find it's not working or we have better workaround
        try
        {
            Thread.sleep(1000);
        }
        catch (InterruptedException e)
        {
            // swallow it
        }
    }
}
