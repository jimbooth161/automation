package com.atlassian.plugin.automation.page.action;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.plugin.automation.page.ModuleKey;

/**
 * @author: mkonecny
 */
@ModuleKey("com.atlassian.plugin.automation.jira-automation-plugin:set-issue-property-action")
public class SetIssuePropertyForm extends ActionForm
{
    public SetIssuePropertyForm(PageElement container, String moduleKey)
    {
        super(container, moduleKey);
    }

    public SetIssuePropertyForm propertyKey(final String propertyKey)
    {
        setActionParam("setPropertyKeyField", propertyKey);
        return this;
    }

    public SetIssuePropertyForm propertyValue(final String propertyValue)
    {
        setActionParam("setPropertyValueField", propertyValue);
        return this;
    }
}
