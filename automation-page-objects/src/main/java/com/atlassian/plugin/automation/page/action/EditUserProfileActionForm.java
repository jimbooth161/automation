package com.atlassian.plugin.automation.page.action;

import com.atlassian.pageobjects.elements.CheckboxElement;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.plugin.automation.page.ModuleKey;
import org.openqa.selenium.By;

/**
 * @author: mkonecny
 */
@ModuleKey("com.atlassian.plugin.automation.jira-automation-plugin:edit-user-profile-action")
public class EditUserProfileActionForm extends ActionForm
{
    public EditUserProfileActionForm(PageElement container, String moduleKey)
    {
        super(container, moduleKey);
    }

    public EditUserProfileActionForm propertyKey(final String propertyKey)
    {
        setActionParam("editUserProfileKeyField", propertyKey);
        return this;
    }

    public EditUserProfileActionForm propertyValue(final String propertyValue)
    {
        setActionParam("editUserProfileValueField", propertyValue);
        return this;
    }

    public EditUserProfileActionForm shouldOverwrite(final boolean shouldOverwrite)
    {
        final CheckboxElement checkbox = container.find(By.name("editUserProfileShouldOverwrite"), CheckboxElement.class);
        if (shouldOverwrite)
        {
            checkbox.check();
        }
        else
        {
            checkbox.uncheck();
        }
        return this;
    }
}
