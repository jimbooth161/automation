package com.atlassian.plugin.automation.jira.spi.scheduler;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.scheduler.cron.CronExpressionValidator;

import javax.inject.Inject;
import javax.inject.Named;

@Named
@ExportAsService
public class JiraCronExpressionValidator
{
    private final CronExpressionValidator cronExpressionValidator;

    @Inject
    public JiraCronExpressionValidator(@ComponentImport CronExpressionValidator cronExpressionValidator)
    {
        this.cronExpressionValidator = cronExpressionValidator;
    }

    public boolean isValidExpression(final String expression)
    {
        return cronExpressionValidator.isValid(expression);
    }
}
