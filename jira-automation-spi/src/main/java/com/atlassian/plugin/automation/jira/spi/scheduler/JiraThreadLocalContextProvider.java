package com.atlassian.plugin.automation.jira.spi.scheduler;

import com.atlassian.jira.util.thread.JiraThreadLocalUtil;
import com.atlassian.plugin.automation.spi.scheduler.ThreadLocalContextProvider;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Local thread context provider for JIRA
 */
@Named
@ExportAsService
public class JiraThreadLocalContextProvider implements ThreadLocalContextProvider
{
    private static final Logger log = Logger.getLogger(JiraThreadLocalContextProvider.class);
    private final JiraThreadLocalUtil jiraThreadLocalUtil;

    @Inject
    public JiraThreadLocalContextProvider(@ComponentImport JiraThreadLocalUtil jiraThreadLocalUtil)
    {
        this.jiraThreadLocalUtil = jiraThreadLocalUtil;
    }

    @Override
    public void preCall()
    {
        jiraThreadLocalUtil.preCall();
    }

    @Override
    public void postCall()
    {
        jiraThreadLocalUtil.postCall(log, new JiraThreadLocalUtil.WarningCallback()
        {
            @Override
            public void onOpenTransaction()
            {
                log.error("A database connection was left open.");
            }
        });
    }
}
